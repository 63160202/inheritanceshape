/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author admin
 */
public class Square extends Rectangle {
    private double s;

    public Square(String name, double s) {
        super(name, s, s);
        this.s = s;
    }

    @Override
    public void ShowArea() {
        if (s <= 0) {
            System.out.println("Side must more than 0!!");
        } else {
            System.out.println("Area of " + name + " : "+ calArea());
        }
    }

}
