/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author admin
 */
public class Triangle extends Shape {
    private double b;
    private double h;

    public Triangle(String name, double b, double h) {
        super(name);
        this.b = b;
        this.h = h;
    }

    @Override
    public double calArea() {
        return 0.5*b*h;
    }

    @Override
    public void ShowArea() {
        if (b <= 0 || h <= 0) {
            System.out.println("Base and Height must more than 0!!");
        } else {
            System.out.println("Area of " + name + " : " + calArea());
        }
    }

}
